crispy-forms-foundation==0.4.1
dj-database-url==0.3.0
Django==1.5.10
django-crispy-forms==1.4.0
django-mailto==0.1.0b0
Pillow==2.0.0
virtualenv==12.1.1
waitress==0.8.9
whitenoise==1.0.6
