import random
import string
### choose_from_hist  
def choose_from_hist(h):
    choix = histogram(h)
    liste =[]
    for lettre, Nbr_Lettre in choix.items():
        liste.extend([lettre]*Nbr_Lettre)
    return random.choice(liste)

## histogram
def histogram(contenu):
        Probabilite = dict()
        for char in contenu:
            if char not in Probabilite:
                Probabilite[char] = 1
            else:
                Probabilite[char] += 1
        return Probabilite
