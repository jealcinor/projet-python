from django.contrib import admin
from philadelphie.models import Personne, Commentateur, Commentaire

class CommentaireAdmin(admin.ModelAdmin):    
	list_display   = ('actualites','pseudo', 'commentaire')    
	list_filter    = ('actualites','date',) 
	search_fields  = ('pseudo', 'actualites')

class PersonneAdmin(admin.ModelAdmin):    
	list_display   = ('pseudo','email', 'password')    
	list_filter    = ('pseudo', 'email',)
	search_fields  = ('pseudo', 'email')

class CommentateurAdmin(admin.ModelAdmin):    
	list_display   = ('titre_article', 'auteur','contenu')    
	list_filter    = ('titre_article', 'auteur','contenu',)
	date_hierarchy = 'date'    
	ordering       = ('date', )
	search_fields  = ('titre_article', 'auteur','contenu')

admin.site.register(Personne,PersonneAdmin) 
admin.site.register(Commentateur, CommentateurAdmin)
admin.site.register(Commentaire, CommentaireAdmin)
