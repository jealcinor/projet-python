from django.conf.urls import patterns, include, url
from django.conf.urls.static import static 
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('philadelphie.views',
					   url(r'^$', 'acceuil'),
					   url(r'^acceuil/$', 'acceuil'),
					   url(r'^apropos/$', 'apropos'),
             		   url(r'^actualites/$', 'actualites'),
                       url(r'^inscrire/$', 'inscrire'),
                       url(r'^connecter/$', 'connecter'),
                       url(r'^envoyer/$', 'envoyer'),
                       url(r'^oukonekte/$', 'oukonekte'),
                       url(r'^deconnexion/$', 'deconnexion'),
                       url(r'^acceuil1/$', 'acceuil1'), 
                       url(r'^articles/(?P<id>\d+)$', 'articles'),
                       url(r'^articles1/(?P<id>\d+)$', 'articles1'),
             url(r'^apropos1/$', 'apropos1'),
             url(r'^actualites1/$', 'actualites1'),
             url(r'^ajouter/$', 'ajouter'),
             url(r'^commenter/$', 'commenter'),
             url(r'^croyances/$', 'croyances'),
             url(r'^croyances1/$', 'croyances1'),
             url(r'^administration/$', 'administration'),
             url(r'^ab/$', 'voir'),
             url(r'^mailto/$', include('mailto.urls')),

    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
            url(r'^admin/', include(admin.site.urls)),
)
