from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Personne(models.Model):
    pseudo = models.CharField(max_length=42)
    email = models.EmailField()
    password = models.CharField(max_length=42)
    def __unicode__(self):
        return self.pseudo

class Envoyeur(models.Model):
    sujet = models.CharField(max_length=42)
    email = models.EmailField()
    message = models.TextField()
    def __unicode__(self):
        return self.email

class Commentateur(models.Model):
    titre_article = models.TextField()
    auteur = models.CharField(max_length=42)
    contenu = models.TextField()
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    fichier = models.FileField(upload_to="photos/") 
    def __unicode__(self):
        return self.titre_article

class Commentaire(models.Model):
    actualites = models.ForeignKey(Commentateur)
    pseudo = models.CharField(max_length=42)
    date = models.DateTimeField(auto_now_add=True, auto_now=False)
    commentaire = models.TextField()
    def __unicode__(self):
        return self.commentaire