# Create your views here.
#-*- coding: utf-8 -*
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from philadelphie.form import PersonneForm
from philadelphie.form import EnvoyeurForm
from philadelphie.form import CommentForm
from philadelphie.form import AjouterForm
from django.contrib.auth import authenticate, login
from datetime import datetime
from datetime import date
import calendar
from django.core.mail import send_mail
from django.core.context_processors import csrf
from philadelphie.models import Commentateur
from philadelphie.models import Commentaire
from philadelphie.models import Personne
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
import webbrowser
from models import Personne
from django.contrib.auth.models import User
from django.contrib.auth.views import logout
from django.core.files import File
from mailto import mailto

def acceuil(request):
     cal = calendar.prmonth(1,1)
     date = datetime.now()
     texte1 ="Les adventistes du septième jour reconnaissent la Bible comme leur seul credo et professent un certain nombre\
      de croyances fondamentales procédant des Saintes Écritures. Ces croyances, énoncées ci-après, exposent la manière dont \
      l´Église conçoit et exprime l´enseignement biblique. Cette profession de foi est susceptible d´être révisée lors d´une \
      session de la Conférence Générale, quand l´Église est amenée par l´Esprit Saint à une meilleure compréhension des vérités \
      bibliques ou qu´elle trouve un langage mieux approprié pour exprimer ce que la Parole de Dieu enseigne.Les Saintes Écritures\
      1. Les Saintes Écritures – l´Ancien et le Nouveau Testament – sont la Parole de Dieu écrite, communiquée grâce à l´inspiration divine \
      par l´intermédiaire de saints hommes de Dieu qui ont parlé et écrit sous l´impulsion du Saint-Esprit. Dans cette Parole, Dieu a confié \
      à l´homme la connaissance nécessaire au salut. Les Saintes Écritures constituent la révélation infaillible de sa volonté. Elles sont la \
      norme du caractère, le critère de l´expérience, le fondement souverain des doctrines et le récit digne de confiance des interventions de\
      Dieu dans l´histoire (2 P 1.20, 21 ; 2 Tm 3.16, 17 ; Ps 119.105 ; Pr 30.5, 6 ; Es 8.20 ; Jn 17.17. 1 Th 2.13 ; Hé 4.12)."

     texte2 ="Avant les gens habitant à Sarthe n’avait qu’un lieu d’adoration. Nous adorions à mamré (Église Adventiste de Drouillard).\
     Certains ont eu l’idée de créer une école sabbat. Serge un ancien respectable et respectés décida de faire cette école sabbat en  \
     nous donnant un local ou on entrepris le nom de Mamré 2.\
     Ayant comme mission de prêcher l’évangile, cette école Sabbat devint une  église n’étant plus sur la tutelle de Mamré, nous avons \
     pris l’appellation de Philadelphie de Sarthe (Amour fraternelle).\
     Tout comme toute autre église adventiste, Philadelphie de Sarthe est composé de plusieurs départements qui sont :\
     A.  L’école du sabbat\
     B.  Les activités Laïques\
     C.  Jeunesse(JA)\
     D.  Sante et Tempérance\
     E.  Enfants, Femmes et famille\
     F.  Bienfaisance (Dorcas, bon samaritain)\
     G.  Communication\
     H.  Publication\
     I.  Éducation\
     Telle est l’origine, l’histoire de notre dénomination. Elle repose sur la vérité des Saintes écritures, la foi en Jésus \
     ainsi qu’en son retour et l’observation de la loi de Dieu y compris l’observation Du saint sabbat de l’éternel."

     article = Commentateur.objects.all().order_by('date')
     articles = article.reverse()[:1]
     return render(request, 'acceuil.html',locals())


def apropos(request):
     formenvoyeur =EnvoyeurForm()
     return render(request, 'apropos.html', {'formenvoyeur': formenvoyeur,})


def inscrire (request):
    registered = False
    if request.method == 'POST':
        pseudo = request.POST['pseudo']
        email = request.POST['email']
        password = request.POST['password']
        form = PersonneForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(pseudo, email,password,) 
            form.save()
            return render(request, 'oukonekte.html',locals())
        else:
            print(form.errors)
    else:
        form = PersonneForm()
    var = {'form': form,'username': request.user.username }
    var.update(csrf(request))
    return render_to_response('formulaire.html', var)



def connecter(request):
    article = Commentateur.objects.all().order_by('date')
    articles = article.reverse()
    commentaires = Commentaire.objects.all().reverse()
    if request.method == 'POST':
        username = request.POST['pseudo']
        password = request.POST['pass']
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            formcomment = CommentForm(initial={'pseudo':request.user })
            return render(request, 'actualites1.html', locals())
        else:
            error = "Aucun champs ne peut etre vide ou Username ou/et password inexistant"
    else:
        return render(request, 'formlogin.html',locals())
    return render(request, 'formlogin.html',locals())


def  envoyer(request):
    if request.method == 'POST':
          sujet = request.POST['sujet']
          sender = request.POST['email']
          msg_user = request.POST['message']
          destinataires = ["jean_net@live.fr"]
          formenvoyeur = EnvoyeurForm()
          if sujet and sender and msg_user:
            send_mail(sujet, msg_user, sender, ['jean_net@live.fr'], connection=None)
            #send_mail(sujet,msg_user,sender,destinataires,fail_silently=False, auth_user=None, auth_password=None, connection=None, html_message=None)
            #mailto(destinataires,sender)
            no_errors = "Message envoye" 
            return render (request, 'apropos.html', locals())
          else:
            errors ="Non envoye"
    else:
        return render (request, 'apropos.html', locals())
    return render (request, 'apropos.html', locals())



def  actualites(request):
    formcomment = CommentForm(request.POST)
    article = Commentateur.objects.all().order_by('date')
    articles = article.reverse()
    commentaires = Commentaire.objects.all().order_by('date')
    commentaires = commentaires.reverse()
    return render_to_response('actualites.html', locals())


def  oukonekte (request):
    return render(request, 'oukonekte.html',locals())

def deconnexion(request):
    logout(request) 
    return render(request, 'formlogin.html',locals())


def acceuil1(request):
     cal = calendar.prmonth(1,1)
     date = datetime.now()
     texte1 ="Les adventistes du septième jour reconnaissent la Bible comme leur seul credo et professent un certain nombre\
      de croyances fondamentales procédant des Saintes Écritures. Ces croyances, énoncées ci-après, exposent la manière dont \
      l´Église conçoit et exprime l´enseignement biblique. Cette profession de foi est susceptible d´être révisée lors d´une \
      session de la Conférence Générale, quand l´Église est amenée par l´Esprit Saint à une meilleure compréhension des vérités \
      bibliques ou qu´elle trouve un langage mieux approprié pour exprimer ce que la Parole de Dieu enseigne.Les Saintes Écritures\
      1. Les Saintes Écritures – l´Ancien et le Nouveau Testament – sont la Parole de Dieu écrite, communiquée grâce à l´inspiration divine \
      par l´intermédiaire de saints hommes de Dieu qui ont parlé et écrit sous l´impulsion du Saint-Esprit. Dans cette Parole, Dieu a confié \
      à l´homme la connaissance nécessaire au salut. Les Saintes Écritures constituent la révélation infaillible de sa volonté. Elles sont la \
      norme du caractère, le critère de l´expérience, le fondement souverain des doctrines et le récit digne de confiance des interventions de\
      Dieu dans l´histoire (2 P 1.20, 21 ; 2 Tm 3.16, 17 ; Ps 119.105 ; Pr 30.5, 6 ; Es 8.20 ; Jn 17.17. 1 Th 2.13 ; Hé 4.12)."

     texte2 ="Avant les gens habitant à Sarthe n’avait qu’un lieu d’adoration. Nous adorions à mamré (Église Adventiste de Drouillard).\
     Certains ont eu l’idée de créer une école sabbat. Serge un ancien respectable et respectés décida de faire cette école sabbat en  \
     nous donnant un local ou on entrepris le nom de Mamré 2.\
     Ayant comme mission de prêcher l’évangile, cette école Sabbat devint une  église n’étant plus sur la tutelle de Mamré, nous avons \
     pris l’appellation de Philadelphie de Sarthe (Amour fraternelle).\
     Tout comme toute autre église adventiste, Philadelphie de Sarthe est composé de plusieurs départements qui sont :\
     A.  L’école du sabbat\
     B.  Les activités Laïques\
     C.  Jeunesse(JA)\
     D.  Sante et Tempérance\
     E.  Enfants, Femmes et famille\
     F.  Bienfaisance (Dorcas, bon samaritain)\
     G.  Communication\
     H.  Publication\
     I.  Éducation\
     Telle est l’origine, l’histoire de notre dénomination. Elle repose sur la vérité des Saintes écritures, la foi en Jésus \
     ainsi qu’en son retour et l’observation de la loi de Dieu y compris l’observation Du saint sabbat de l’éternel."

     article = Commentateur.objects.all().order_by('date')
     articles = article.reverse()[:1]
     return render(request, 'acceuil1.html',locals())


def apropos1(request):
     formenvoyeur =EnvoyeurForm()
     return render(request, 'apropos1.html', {'formenvoyeur': formenvoyeur,})

def  actualites1(request):
    formcomment = CommentForm(initial={'pseudo':request.user })
    article = Commentateur.objects.all().order_by('date')
    articles = article.reverse()
    commentaires = Commentaire.objects.all().order_by('date')
    commentaires = commentaires.reverse()
    return render(request,'actualites1.html', locals())


def  ajouter(request):
    registered = False
    if request.method == 'POST':
        formajout = AjouterForm(request.POST, request.FILES)
        if formajout.is_valid():
            commentateur = Commentateur()
            titre = request.POST['titre_article']
            auteur = request.POST['auteur']
            contenu = request.POST['contenu']
            fichier = formajout.cleaned_data['fichier']
            formajout.save()
            registered = True
            return render(request, 'oukonekte.html',locals())
        else:
            print(formajout.errors)
    else:
       formajout = AjouterForm ()
    var = {'formajout': formajout}
    var.update(csrf(request))
    return render_to_response('ajouter.html', var)


def croyances(request):
    return render(request, 'croyances.html',)

def croyances1(request):
    return render(request, 'croyances1.html',)

def administration(request):
    formajout = AjouterForm(request.POST)
    return render(request, 'ajouter.html',locals())

def voir(request):    
    cont = Commentateur.objects.all()    
    return render(request, 'ab.html',{'cont':cont})

def articles(request, id):
    try:
        articles = Commentateur.objects.get(id=id)
        commentaire =Commentaire.objects.filter(actualites=id).order_by('date')
        commentaire = commentaire.reverse()
    except Commentateur.DoesNotExist:
        raise Http404
    formcomment = CommentForm(initial={'pseudo':request.user})
    return render(request, 'articles.html', locals())


def articles1(request, id):
    try:
        articles = Commentateur.objects.get(id=id)
        commentaire =Commentaire.objects.filter(actualites=id).order_by('date')
        commentaire = commentaire.reverse()
    except Commentateur.DoesNotExist:
        raise Http404
    formcomment = CommentForm(initial={'pseudo':request.user})
    return render(request, 'articles1.html', locals())


def commenter(request):
    pseudo = request.user
    erreur = False
    if request.method == 'POST':
        evenement = request.POST['actualites']
        pseudo = request.POST['pseudo']
        comment = request.POST['commentaire']
        formcomment = CommentForm(request.POST)
        if formcomment.is_valid():
            formcomment.save()
            message= "Nouveau commentaire enregistré"
        else:
            erreur = True
    else:
        formcomment = CommentForm()
    article = Commentateur.objects.all().order_by('date')
    articles = article.reverse()
    commentaires = Commentaire.objects.all().order_by('date')
    commentaires = commentaires.reverse()
    var = {'formcomment':formcomment,'articles':articles, 'commentaires':commentaires}
    var.update(csrf(request))
    return render(request,'actualites1.html', locals())