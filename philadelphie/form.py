from django import forms
from models import Personne
from models import Envoyeur
from models import Commentateur
from models import Commentaire

class PersonneForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())
	class Meta:
		model = Personne

class EnvoyeurForm(forms.ModelForm):
	class Meta:
		model = Envoyeur


class CommentForm(forms.ModelForm):
	pseudo = forms.CharField(widget=forms.TextInput(attrs={'readonly': True}),initial='')
	class Meta:
		model = Commentaire
		#exclude = ('actualites')


class AjouterForm(forms.ModelForm):
	class Meta:
		model = Commentateur
