import os
import string

os.chdir('C:\\Users\\Jeannet\\Desktop\\Test')
with open('pg47954.txt','r') as fichier:
    dic = {}
    contenu = fichier.read() #Lit le fichier
    contenu = contenu.lower() #Miniscule
    contenu = contenu.replace(" ",".")
    for lignes in contenu:
         if lignes in string.punctuation or lignes in string.whitespace:
                contenu = contenu.replace(lignes," ") # Effacer les ponctuations
    contenu = contenu.split() #Effacer les espaces blanches
    Nbre_Mot = len(contenu)
    
### Compter le nombre de fois que chaques mots sont repetes
    for mot in contenu:
        if mot not in dic:
            dic[mot] = 1
        else:
            dic[mot] += 1
            
print dic
print "Le livre contient {} mots.".format(Nbre_Mot)
print "Le livre contient {} mots differents.".format(len(dic))
