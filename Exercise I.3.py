import os
import string

os.chdir('C:\\Users\\Jeannet\\Desktop\\Test')
with open('pg47954.txt','r') as fichier:
    dic = {}
    l = []
    lm = []
    a =0
    contenu = fichier.read() #Lit le fichier
    contenu = contenu.lower() #Miniscule
    contenu = contenu.replace(" ",".")
    for lignes in contenu:
         if lignes in string.punctuation or lignes in string.whitespace:
                contenu = contenu.replace(lignes," ") # Effacer les ponctuations
    contenu = contenu.split() #Effacer les espaces blanches
    Nbre_Mot = len(contenu)
    
### Compter le nombre de fois que les mots sont repetes
    for mot in contenu:
        if mot not in dic:
            dic[mot] = 1
        else:
            dic[mot] += 1
### Affichage des 20 mots les plus repetes
    for m,i in dic.items():
         l.extend([(m,i)])
         linverse = [(i,m) for m,i in l]
         linverse = sorted (linverse)
         linverse.reverse()
         l = [(m,i) for i,m in linverse]
         l = l[:20]
    print l
          
